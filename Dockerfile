FROM golang:1.6.1
MAINTAINER emmanuel.roullit@gmail.com

RUN \
  apt-get update -yq && \
  apt-get install -yq --no-install-recommends \
    autoconf \
    automake \
    build-essential \
    git \
    libtool \
    nano \
    vim-tiny \
    unzip && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN \
  curl -O --insecure https://bootstrap.pypa.io/get-pip.py && \
  python get-pip.py && \
  pip install protobuf==3.0.0b2 googleapis-common-protos

RUN \
  GIT_SSL_NO_VERIFY=true git clone --recursive -b release-0_13_1 https://github.com/grpc/grpc.git && \
  cd grpc && \
  make && \
  cd third_party/protobuf && \
  make install && \
  cd - && \
  make install && \
  pip install -r requirements.txt && \
  GRPC_PYTHON_BUILD_WITH_CYTHON=1 pip install .

RUN \
  go get \
    github.com/golang/protobuf/proto \
    github.com/golang/protobuf/protoc-gen-go \
    google.golang.org/grpc \
    github.com/gengo/grpc-gateway/protoc-gen-grpc-gateway \
    github.com/gengo/grpc-gateway/protoc-gen-swagger \
    go.pedge.io/protoeasy/cmd/protoeasy
