This docker images contains:
 * protobuf 3 compiler
 * gRPC libraries
 * grpc-gateway
 * protobuf swagger generator
 * python protobuf support
 * go protobuf support
